/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2021  SR_team me@sr.team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "NoRecoil.h"

#include <cstdint>
#include <dlfcn.h>
#include <sys/mman.h>
#include <sys/user.h>

NoRecoil g_instance;

NoRecoil::NoRecoil()
{
    auto gtasa = dlopen("libGTASA.so", RTLD_NOW);
    pfPlayerAimScale = (float*)dlsym(gtasa, "fPlayerAimScale");
    Dl_info info;
    dladdr((void*)pfPlayerAimScale, &info);
    mprotect((void*)(uintptr_t(pfPlayerAimScale) & 0xFFFFF000), PAGE_SIZE, PROT_READ | PROT_WRITE | PROT_EXEC);
    orig_fPlayerAimScale = *pfPlayerAimScale;
    *pfPlayerAimScale = 0.0f;
}

NoRecoil::~NoRecoil()
{
    *pfPlayerAimScale = orig_fPlayerAimScale;
}
